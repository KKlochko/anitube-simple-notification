* Change Log
** 0.1.0 <2022-07-08>
   First release
** 0.1.1 <2022-07-08>
   New small feature: update anime status
** 0.1.2 <2022-07-08>
   New small feature: get an anime entry
** 0.1.3 <2022-07-08>
   New small feature: add an anime entry if
** 0.2.0 <2022-07-09>
   New feature: get an anime info from page
** 0.3.0 <2022-07-09>
   New feature: send a notification
** 0.4.2 <2022-07-09>
   New feature: check for a new url and for the updated state
   New small feature: add the urls config file
   Little Fix: add update status in quotes
** 0.4.3 <2022-07-09>
   New small feature: repeating checking the content.
** 0.5.1 <2022-07-10>
   New feature: download posters and use them as icons for notifications.
   New small feature: showing information about a current task.
** 0.5.2 <2022-07-21>
   Added pyproject.toml.
   Added img/ folder.
** 0.6.0 <2022-08-11>
   Updated the cli interface
** 1.0.1 <2023-01-14>
   Added TOML configuration file.
   Updated paths of files and folders.
   Updated my name to my fullname and release years.
** 1.0.2 <2023-01-15>
   Refactor the database class and remove function for the creating file.
** 1.1.1 <2023-02-28>
   Added os paths for the data and the config by default.
   Added if config is missing, then the hint shows its path.
   Updated pyproject.toml information.
** 1.1.2 <2023-07-16>
   Add the requirements and .gitignore.
** 1.1.3 <2023-07-16>
   Update the mail information.
** 1.1.4 <2023-07-16>
   Update the package information and imports.
** 1.2.0 <2023-07-16>
   Add commands to execute the package.
** 1.2.1 <2023-07-16>
   Remove the README.org.
** 1.2.2 <2023-07-16>
   Add the CI/CD configuration.
** 1.2.3 <2023-07-16>
   Fix the command that was "ans", but it must be "asn".
** 1.2.4 <2023-07-16>
   Update the README.
